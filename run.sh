#!/bin/sh

# -----------------------------------------------------------------------------------------
# Purpose:	Run Veg_Hydrodynamics using specified options
#
# Note: 	Input / output paths within the program are relative, requiring the program
#		to be run from the top directory either from this script or the command line
# -----------------------------------------------------------------------------------------

# Options
BIN="bin/Veg_Hydrodynamics"
DOWEL="--rod-type=dowel"
CORNUS="--rod-type=cornus"
SPARSE="--spacing=sparse"
DENSE="--spacing=dense"
LOW="--depth=low"
HIGH="--depth=high"
OPT="--calc-edv --calc-tke"

# Run program for each dowell density / depth combination
A="$BIN $DOWEL $SPARSE $LOW $OPT"
echo "\nRUNNING:\n"$A"\n"
if $A; then
	echo "\nPROGRAM COMPLETE!\n\n"
else
	echo "\nERROR - PROGRAM TERMINATED!\n\n"
fi

B="$BIN $DOWEL $SPARSE $HIGH $OPT"
echo "\n\nRUNNING:\n"$B"\n"
if $B; then
	echo "\nPROGRAM COMPLETE!\n\n"
else
	echo "\nERROR - PROGRAM TERMINATED!\n\n"
fi

C="$BIN $DOWEL $DENSE $LOW $OPT"
echo "\n\nRUNNING:\n"$C"\n"
if $C; then
	echo "\nPROGRAM COMPLETE!\n\n"
else
	echo "\nERROR - PROGRAM TERMINATED!\n\n"
fi

D="$BIN $DOWEL $DENSE $HIGH $OPT"
echo "\n\nRUNNING:\n"$D"\n"
if $D; then
	echo "\nPROGRAM COMPLETE!\n\n"
else
	echo "\nERROR - PROGRAM TERMINATED!\n\n"
fi
