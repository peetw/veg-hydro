#include "TKE.h"

TKE::TKE( const Params &rParams )
{
	// Assign TKE array
	mTke = vector3d (rParams.rows(), vector2d(rParams.cols(), vector1d(rParams.layers(), 0.0)));

	// Assign TKE budget arrays
	mAdvection = v3dCartesian (rParams.rows(), v2dCartesian(rParams.cols(), v1dCartesian(rParams.layers())));
	mProduction = v3dTensor (rParams.rows(), v2dTensor(rParams.cols(), v1dTensor(rParams.layers())));
	mTurbTrans = v3dCartesian (rParams.rows(), v2dCartesian(rParams.cols(), v1dCartesian(rParams.layers())));
	mViscTrans = v3dCartesian (rParams.rows(), v2dCartesian(rParams.cols(), v1dCartesian(rParams.layers())));
}


void TKE::calcTke( const Params &rParams, const Data &rData )
{
	// Calculate turbulent kinetic energy
	for( unsigned int row = 0; row < rParams.rows(); row++ )
		for( unsigned int col = 0; col < rParams.cols(); col++ )
			for( unsigned int layer = 0; layer < rParams.layers(); layer++ )
			{
				if( is_dowel(row,col) == false )
				{
					// Reynolds stresses
					const Tensor& re = rData.getRe(row,col,layer);

					// Turbulent kinetic energy
					mTke[row][col][layer] = 0.5 * ( re.xx + re.yy + re.zz );

				}
			}
}


void TKE::calcAdvection( const Params &rParams, const Data &rData )
{
	// Calculate turbulent energy advection by mean flow
	for( unsigned int row = 0; row < rParams.rows(); row++ )
		for( unsigned int col = 0; col < rParams.cols(); col++ )
			for( unsigned int layer = 0; layer < rParams.layers(); layer++ )
			{
				if( is_dowel(row,col) == false )
				{
					// Mean velocities
					const Cartesian& vel = rData.getVel(row,col,layer);

					// TKE gradients
					const double delkx = calcTkeGrad( rParams, 'x', row, col, layer );
					const double delky = calcTkeGrad( rParams, 'y', row, col, layer );
					const double delkz = calcTkeGrad( rParams, 'z', row, col, layer );

					// Calculate TKE advection
					mAdvection[row][col][layer].x = - vel.x * delkx;
					mAdvection[row][col][layer].y = - vel.y * delky;
					mAdvection[row][col][layer].z = - vel.z * delkz;
				}
			}
}


void TKE::calcProduction( const Params &rParams, const Data &rData )
{
	// Calculate turbulent energy production
	for( unsigned int row = 0; row < rParams.rows(); row++ )
		for( unsigned int col = 0; col < rParams.cols(); col++ )
			for( unsigned int layer = 0; layer < rParams.layers(); layer++ )
			{
				if( is_dowel(row,col) == false )
				{
					// Reynolds stresses
					const Tensor& re = rData.getRe(row,col,layer);

					// Dilatational production
					const double delUx = rData.calcVelGrad( rParams, 'U', 'x', row, col, layer );
					const double delVy = rData.calcVelGrad( rParams, 'V', 'y', row, col, layer );
					const double delWz = rData.calcVelGrad( rParams, 'W', 'z', row, col, layer );

					mProduction[row][col][layer].xx = - re.xx * delUx;
					mProduction[row][col][layer].yy = - re.yy * delVy;
					mProduction[row][col][layer].zz = - re.zz * delWz;


					// Shear production
					const double delUy = rData.calcVelGrad( rParams, 'U', 'y', row, col, layer );
					const double delUz = rData.calcVelGrad( rParams, 'U', 'z', row, col, layer );

					const double delVx = rData.calcVelGrad( rParams, 'V', 'x', row, col, layer );
					const double delVz = rData.calcVelGrad( rParams, 'V', 'z', row, col, layer );

					const double delWx = rData.calcVelGrad( rParams, 'W', 'x', row, col, layer );
					const double delWy = rData.calcVelGrad( rParams, 'W', 'y', row, col, layer );

					mProduction[row][col][layer].xy = - re.xy * delVx;
					mProduction[row][col][layer].xz = - re.xz * delWx;

					mProduction[row][col][layer].yx = - re.yx * delUy;
					mProduction[row][col][layer].yz = - re.yz * delWy;

					mProduction[row][col][layer].zx = - re.zx * delUz;
					mProduction[row][col][layer].zy = - re.zy * delVz;
				}
			}
}


void TKE::calcTransport( const Params &rParams, const Data &rData )
{
	// Calculate turbulent energy transport
	for( unsigned int row = 0; row < rParams.rows(); row++ )
		for( unsigned int col = 0; col < rParams.cols(); col++ )
			for( unsigned int layer = 0; layer < rParams.layers(); layer++ )
			{
				if( is_dowel(row,col) == false )
				{
					// Turbulent transport of TKE by fluctuating velocity gradients
					const double delukx = rData.calcFluctTkeGrad( rParams, 'u', 'x', row, col, layer );
					const double delvky = rData.calcFluctTkeGrad( rParams, 'v', 'y', row, col, layer );
					const double delwkz = rData.calcFluctTkeGrad( rParams, 'w', 'z', row, col, layer );

					mTurbTrans[row][col][layer].x = - delukx;
					mTurbTrans[row][col][layer].y = - delvky;
					mTurbTrans[row][col][layer].z = - delwkz;

					// Viscous transport of TKE by 2nd order TKE gradients
					const double del2kx = calcTke2ndGrad( rParams, 'x', row, col, layer );
					const double del2ky = calcTke2ndGrad( rParams, 'y', row, col, layer );
					const double del2kz = calcTke2ndGrad( rParams, 'z', row, col, layer );

					mViscTrans[row][col][layer].x = KIN_VISC * del2kx;
					mViscTrans[row][col][layer].y = KIN_VISC * del2ky;
					mViscTrans[row][col][layer].z = KIN_VISC * del2kz;
				}
			}
}


double TKE::calcTkeGrad(	const Params &rParams, const char dir,
							const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	// Check string args ok
	if( dir != 'x' && dir != 'y' && dir != 'z' )
		exit(EXIT_FAILURE);

	// Initialize gradient return value
	double grad = 0.0;

	// Determine gradients
	if( dir == 'x' )
	{
		if( row == rParams.rows()-1 || is_dowel(row+1,col) )
			// Backward difference
			grad = (mTke[row][col][layer] - mTke[row-1][col][layer]) / rParams.pointSpacingX();
		else if( row == 0 || is_dowel(row-1,col) )
			// Forward difference
			grad = (mTke[row+1][col][layer] - mTke[row][col][layer]) / rParams.pointSpacingX();
		else
			// Cetral difference
			grad = (mTke[row+1][col][layer] - mTke[row-1][col][layer]) / (2.0*rParams.pointSpacingX());
	}
	else if( dir == 'y' )
	{
		if( col == rParams.cols()-1 ||is_dowel(row,col+1) )
			// Backward difference
			grad = (mTke[row][col][layer] - mTke[row][col-1][layer]) / rParams.pointSpacingY();
		else if( col == 0 || is_dowel(row,col-1) )
			// Forward difference
			grad = (mTke[row][col+1][layer] - mTke[row][col][layer]) / rParams.pointSpacingY();
		else
			// Cetral difference
			grad = (mTke[row][col+1][layer] - mTke[row][col-1][layer]) / (2.0*rParams.pointSpacingY());
	}
	else if( dir == 'z' )
	{
		if( layer == 0 )
			// Backward difference (Assume no-slip boundary, ie. U=0 on bed)
			grad = mTke[row][col][layer] / rParams.pointSpacingZ0();
		else if( layer == rParams.layers()-1 )
			// Backward difference
			grad = (mTke[row][col][layer] - mTke[row][col][layer-1]) / rParams.pointSpacingZ();
		else
			// Cetral difference
			grad = (mTke[row][col][layer+1] - mTke[row][col][layer-1]) / (2.0*rParams.pointSpacingZ());
	}

	return grad;
}


double TKE::calcTke2ndGrad(	const Params &rParams, const char dir,
							const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	// Check string args ok
	if( dir != 'x' && dir != 'y' && dir != 'z' )
		exit(EXIT_FAILURE);

	// Initialize gradient return value
	double grad = 0.0;

	// Determine gradients
	if( dir == 'x' )
	{
		if( row == 0 || is_dowel(row-1,col) )
			// Forward difference
			grad = (mTke[row+2][col][layer] - 2.0*mTke[row+1][col][layer] + mTke[row][col][layer]) / (rParams.pointSpacingX()*rParams.pointSpacingX());
		else if( row == rParams.rows()-1 || is_dowel(row+1,col) )
			// Backward difference
			grad = (mTke[row][col][layer] - 2.0*mTke[row-1][col][layer] + mTke[row-2][col][layer]) / (rParams.pointSpacingX()*rParams.pointSpacingX());
		else
			// Central difference
			grad = (mTke[row+1][col][layer] - 2.0*mTke[row][col][layer] + mTke[row-1][col][layer]) / (rParams.pointSpacingX()*rParams.pointSpacingX());
	}
	else if( dir == 'y' )
	{
		if( col == 0 || is_dowel(row,col-1) )
			// Forward difference
			grad = (mTke[row][col+2][layer] - 2.0*mTke[row][col+1][layer] + mTke[row][col][layer]) / (rParams.pointSpacingY()*rParams.pointSpacingY());
		else if( col == rParams.cols()-1 || is_dowel(row,col+1) )
			// Backward difference
			grad = (mTke[row][col][layer] - 2.0*mTke[row][col-1][layer] + mTke[row][col-2][layer]) / (rParams.pointSpacingY()*rParams.pointSpacingY());
		else
			// Central difference
			grad = (mTke[row][col+1][layer] - 2.0*mTke[row][col][layer] + mTke[row][col-1][layer]) / (rParams.pointSpacingY()*rParams.pointSpacingY());
	}
	else if( dir == 'z' )
	{
		if( layer == 0 )
			// Central difference (Assume no-slip boundary, ie. U=0 on bed)
			grad = (mTke[row][col][layer+1] - 2.0*mTke[row][col][layer]) / (rParams.pointSpacingZ0()*rParams.pointSpacingZ());
		else if( layer == rParams.layers()-1 )
			// Backward difference
			grad = (mTke[row][col][layer] - 2.0*mTke[row][col][layer-1] + mTke[row][col][layer-2]) / (rParams.pointSpacingZ()*rParams.pointSpacingZ());
		else
			// Central difference
			grad = (mTke[row][col][layer+1] - 2.0*mTke[row][col][layer] + mTke[row][col][layer-1]) / (rParams.pointSpacingZ()*rParams.pointSpacingZ());
	}

	return grad;
}


void TKE::output( const Params &rParams )
{
	// Open output file
	std::string filename = "out/" + rParams.filePrefix() + "_tke.dat";
	std::ofstream FILE(filename.c_str());

	// Write header to file
	FILE	<< "# Measuring position\t\tTKE\t\t\t\t\tAdvection\t\t\t\t\tDilatational production\t\t\t\t\t\t\t\t\t"
			<< "Shear production\t\t\t\t\t\t\tTurbulent transport\t\t\t\t\t\tViscous transport\n"
			<< "# X\tY\tZ\t\tk\t\t\t-U dk/dx\t-V dk/dy\t-W dk/dz\t\t-u'2 dU/dx\t-v'2 dV/dy\t-w'2 dW/dz\t\t"
			<< "-u'v' dV/dx\t-u'w' dW/dx\t-v'u' dU/dy\t-v'w' dW/dy\t-w'u' dU/dz\t-w'v' dV/dz\t\t"
			<< "-du'k/dx\t-dv'k/dy\t-dw'k/dz\t\tnu d2k/dx2\tnu d2k/dy2\tnu d2k/dz2\n";

	// Output TKE and TKE budget terms
	for( unsigned int row = 0; row < rParams.rows(); row++ )
		for( unsigned int col = 0; col < rParams.cols(); col++ )
		{
			for( unsigned int layer = 0; layer < rParams.layers(); layer++ )
			{
				const double depthNorm = (layer*rParams.pointSpacingZ()+rParams.pointSpacingZ0()) / rParams.depth();

				FILE	<< "  " << std::setprecision(2) << row*rParams.pointSpacingY() << "\t" << col*rParams.pointSpacingX() << "\t" << depthNorm << "\t\t"
						<< std::scientific << mTke[row][col][layer] << "\t\t"
						<< mAdvection[row][col][layer].x << "\t" << mAdvection[row][col][layer].y << "\t" << mAdvection[row][col][layer].z << "\t\t"
						<< mProduction[row][col][layer].xx << "\t" << mProduction[row][col][layer].yy << "\t" << mProduction[row][col][layer].zz << "\t\t"
						<< mProduction[row][col][layer].xy << "\t" << mProduction[row][col][layer].xz << "\t"
						<< mProduction[row][col][layer].yx << "\t" << mProduction[row][col][layer].yz << "\t"
						<< mProduction[row][col][layer].zx << "\t" << mProduction[row][col][layer].zy << "\t\t"
						<< mTurbTrans[row][col][layer].x << "\t" << mTurbTrans[row][col][layer].y << "\t" << mTurbTrans[row][col][layer].z << "\t\t"
						<< mTurbTrans[row][col][layer].x << "\t" << mViscTrans[row][col][layer].y << "\t" << mViscTrans[row][col][layer].z
						<< "\n";
				FILE.unsetf(std::ios::scientific);
			}
			FILE << "\n\n";
		}

	// Close file
	FILE.close();
}
