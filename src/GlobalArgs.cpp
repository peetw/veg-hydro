#include "GlobalArgs.h"


void GlobalArgs::displayHelp( char *argv[] )
{
	std::cout	<< "\nUsage: " << argv[0] << " [OPTIONS]\n\n"
			<< "  -r, --rod-type=[dowel,cornus]\n\t\tSpecify rod type (REQUIRED)\n"
			<< "  -s, --spacing=[sparse,dense]\n\t\tPlanting density (REQUIRED)\n"
			<< "  -d, --depth=[low,high]\n\t\tFlow depth (REQUIRED)\n"
			<< "  -e, --calc-edv\n\t\tCalculate turbulent eddy viscosities\n"
			<< "  -t, --calc-tke\n\t\tCalculate turbulent kinetic energy\n"
			<< "  -h, --help\n\t\tDisplay this message\n\n\n";
	exit(EXIT_FAILURE);
}

void GlobalArgs::setGlobalArgs( const int argc, char *argv[] )
{
	// Command line options
	const struct option longOpts[] =
	{
		{"help",      no_argument,       NULL, 'h'},
		{"rod-type",  required_argument, NULL, 'r'},
		{"spacing",   required_argument, NULL, 's'},
		{"depth",     required_argument, NULL, 'd'},
		{"calc-edv",  no_argument,       NULL, 'e'},
		{"calc-tke",  no_argument,       NULL, 't'},
		{0,0,0,0},
	};

	// Control params
	int opt = 0;
	int long_index = 0;
	const char *p_opt_string = "r:s:d:eth?";

	// Process the arguments with getopt_long(), then populate globalArgs
	opt = getopt_long( argc, argv, p_opt_string, longOpts, &long_index );
	while( opt != -1 )
	{
		switch( opt )
		{
			case 'r':
				mRodType = std::string(optarg);
				break;

			case 's':
				mSpacing = std::string(optarg);
				break;

			case 'd':
				mDepth = std::string(optarg);
				break;

			case 'e':
				mCalcEdv = true;
				break;

			case 't':
				mCalcTke = true;
				break;

			case 'h':	// Fall-through is intentional
			case '?':
				displayHelp( argv );
				break;
		}

		opt = getopt_long( argc, argv, p_opt_string, longOpts, &long_index );
	}

	// Check that all required arguments have been supplied
	bool args_bad = false;

	if( mRodType == "" || mSpacing == "" || mDepth == "" )
		args_bad = true;
	else if( mRodType != "dowel" && mRodType !=  "cornus" )
		args_bad = true;
	else if( mSpacing != "sparse" && mSpacing != "dense" )
		args_bad = true;
	else if( mDepth != "low" && mDepth != "high" )
		args_bad = true;

	if( args_bad )
		displayHelp( argv );
}
