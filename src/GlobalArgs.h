#ifndef GLOBALARGS_H_
#define GLOBALARGS_H_

#include <unistd.h>
#include <getopt.h>
#include <string>
#include <iostream>
#include <cstdlib>

class GlobalArgs {
	std::string mRodType;
	std::string mSpacing;
	std::string mDepth;
	bool mCalcEdv;
	bool mCalcTke;
public:
	// Constructors
	GlobalArgs() :
		mRodType(""),
		mSpacing(""),
		mDepth(""),
		mCalcEdv(false),
		mCalcTke(false) {
	}

	// Mutators
	void setGlobalArgs( const int, char** );

	// Accessors
	std::string rodType() const { return mRodType; }
	std::string spacing() const { return mSpacing; }
	std::string depth() const { return mDepth; }
	bool calcEdv() const { return mCalcEdv; }
	bool calcTke() const { return mCalcTke; }

	// Operations
	void displayHelp( char** );
};

#endif /* GLOBALARGS_H_ */
