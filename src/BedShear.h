#ifndef BEDSHEAR_H_
#define BEDSHEAR_H_

#include <cmath>
#include <string>
#include <fstream>

#include "vectorTypes.h"
#include "Params.h"
#include "Data.h"
#include "TKE.h"
#include "utils.h"

class BedShear {
	// Members
	vector2d mLogShear;
	vector2d mReShear;
	vector2d mTkeShear;
	vector2d mChezyShear;

	// Operations
	double calcAvrg(const Params&, const vector2d&);
public:
	// Constructor
	BedShear(const Params&);

	// Mutators
	void calcLogShear(const Params&, const Data&);
	void calcReShear(const Params&, const Data&);
	void calcTkeShear(const Params&, const TKE&);
	void calcChezyShear(const Params&, const Data&);

	// Accessors
	double getLogShear(const unsigned int, const unsigned int) const;
	double getReShear(const unsigned int, const unsigned int) const;
	double getTkeShear(const unsigned int, const unsigned int) const;
	double getChezyShear(const unsigned int, const unsigned int) const;

	// Operations
	void output(const Params&);
};


// Inline accessors
inline double BedShear::getLogShear(const unsigned int row, const unsigned int col) const
{
	return mLogShear[row][col];
}

inline double BedShear::getReShear(const unsigned int row, const unsigned int col) const
{
	return mReShear[row][col];
}

inline double BedShear::getTkeShear(const unsigned int row, const unsigned int col) const
{
	return mTkeShear[row][col];
}

inline double BedShear::getChezyShear(const unsigned int row, const unsigned int col) const
{
	return mChezyShear[row][col];
}

#endif /* BEDSHEAR_H_ */
