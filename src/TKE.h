#ifndef TKE_H_
#define TKE_H_

#include <string>
#include <fstream>
#include <iomanip>

#include "vectorTypes.h"
#include "Params.h"
#include "Data.h"
#include "utils.h"

class TKE {
	vector3d mTke;
	v3dCartesian mAdvection;
	v3dTensor mProduction;
	v3dCartesian mTurbTrans;
	v3dCartesian mViscTrans;
public:
	// Constructors
	TKE( const Params& );

	// Accessors
	double getTke( const unsigned int, const unsigned int, const unsigned int ) const;
	const Cartesian& getAdvection( const unsigned int, const unsigned int, const unsigned int ) const;
	const Tensor& getProduction( const unsigned int, const unsigned int, const unsigned int ) const;
	const Cartesian& getTurbTrans( const unsigned int, const unsigned int, const unsigned int ) const;
	const Cartesian& getViscTrans( const unsigned int, const unsigned int, const unsigned int ) const;

	// Mutators
	void calcTke( const Params&, const Data& );
	void calcAdvection( const Params&, const Data& );
	void calcProduction( const Params&, const Data& );
	void calcTransport( const Params&, const Data& );


	// Operations
	double calcTkeGrad( const Params&, const char, const unsigned int, const unsigned int, const unsigned int ) const;
	double calcTke2ndGrad( const Params&, const char, const unsigned int, const unsigned int, const unsigned int ) const;
	void output( const Params& );
};


// Inline accessors
inline double TKE::getTke( const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	return mTke[row][col][layer];
}

inline const Cartesian& TKE::getAdvection( const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	return mAdvection[row][col][layer];
}

inline const Cartesian& TKE::getTurbTrans( const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	return mTurbTrans[row][col][layer];
}

inline const Cartesian& TKE::getViscTrans( const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	return mViscTrans[row][col][layer];
}

inline const Tensor& TKE::getProduction( const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	return mProduction[row][col][layer];
}


#endif /* TKE_H_ */
