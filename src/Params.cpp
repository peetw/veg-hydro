#include "Params.h"

Params::Params() :
	mFilePrefix(""),
	mRows(0),
	mCols(0),
	mLayers(0),
	mDowelSpacingX(0.0),
	mDowelSpacingY(0.0),
	mPointSpacingX(0.0),
	mPointSpacingY(0.0),
	mPointSpacingZ0(0.0),
	mPointSpacingZ(0.0),
	mDepth(0.0),
	mFlowrate(0.0) {
}

void Params::setFilePrefix( const GlobalArgs &rGlobalArgs )
{
	// Rod type
	if( rGlobalArgs.rodType() == "dowel" )
	{
		mFilePrefix = "D";
	}
	else if( rGlobalArgs.rodType() == "cornus" )
	{
		mFilePrefix = "V";
	}

	// Planting density
	if( rGlobalArgs.spacing() == "sparse" )
	{
		mFilePrefix += "2";
	}
	else if( rGlobalArgs.spacing() == "dense" )
	{
		mFilePrefix += "3";
	}

	// Flow _depth
	if( rGlobalArgs.depth() == "low" )
	{
		mFilePrefix += "b";
	}
	else if( rGlobalArgs.depth() == "high" )
	{
		mFilePrefix += "c";
	}
}

void Params::setPointSpacing()
{
	if( mFilePrefix.substr(1,1) == "2" )
	{
		mDowelSpacingX = 0.42;
		mDowelSpacingY = 0.24;

		mPointSpacingX = 0.02;
		mPointSpacingY = 0.02;
	}
	else if( mFilePrefix.substr(1,1) == "3" )
	{
		mDowelSpacingX = 0.21;
		mDowelSpacingY = 0.12;

		mPointSpacingX = 0.01;
		mPointSpacingY = 0.01;
	}

	mPointSpacingZ0 = 0.0075;
	mPointSpacingZ = 0.02;

	mRows = 11;
	mCols = 7;
}

void Params::setFlowProp()
{
	if( mFilePrefix == "D2b" )
	{
		mFlowrate = 0.017;
		mDepth = 0.135;
		mLayers = 5;
	}
	else if( mFilePrefix == "D2c" )
	{
		mFlowrate = 0.029;
		mDepth = 0.235;
		mLayers = 10;
	}
	else if( mFilePrefix == "D3b" )
	{
		mFlowrate = 0.011;
		mDepth = 0.175;
		mLayers = 7;
	}
	else if( mFilePrefix == "D3c" )
	{
		mFlowrate = 0.014;
		mDepth = 0.215;
		mLayers = 10;
	}
}
