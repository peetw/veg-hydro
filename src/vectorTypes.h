#ifndef VECTORTYPES_H_
#define VECTORTYPES_H_

#include <vector>

// Standard 3d array of doubles
typedef std::vector< double > vector1d;
typedef std::vector< std::vector< double > > vector2d;
typedef std::vector< std::vector< std::vector< double > > > vector3d;


// 3d array of cartesian vectors, i.e. x, y, z points
struct Cartesian {
	double x;
	double y;
	double z;
	Cartesian() : x(0.0), y(0.0), z(0.0) {}
};

typedef std::vector< Cartesian > v1dCartesian;
typedef std::vector< std::vector< Cartesian > > v2dCartesian;
typedef std::vector< std::vector< std::vector< Cartesian > > > v3dCartesian;


// 3d array of cartesian stress tensors
struct Tensor {
	double xx;
	double xy;
	double xz;
	double yx;
	double yy;
	double yz;
	double zx;
	double zy;
	double zz;
	Tensor() : xx(0.0),xy(0.0),xz(0.0), yx(0.0),yy(0.0),yz(0.0), zx(0.0),zy(0.0),zz(0.0) {}
};

typedef std::vector< Tensor > v1dTensor;
typedef std::vector< std::vector< Tensor > > v2dTensor;
typedef std::vector< std::vector< std::vector< Tensor > > > v3dTensor;


#endif /* VECTORTYPES_H_ */
