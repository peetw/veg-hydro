#include "BedShear.h"

BedShear::BedShear(const Params &rParams)
{
	// Initialize bed shear stress arays
	mLogShear = vector2d(rParams.rows(), vector1d(rParams.cols(), 0.0));
	mReShear = vector2d(rParams.rows(), vector1d(rParams.cols(), 0.0));
	mTkeShear = vector2d(rParams.rows(), vector1d(rParams.cols(), 0.0));
	mChezyShear = vector2d(rParams.rows(), vector1d(rParams.cols(), 0.0));
}

void BedShear::calcLogShear(const Params &rParams, const Data &rData)
{
	double shear_vel = 0.0;

	for (unsigned int row = 0; row < rParams.rows(); row++)
		for (unsigned int col = 0; col < rParams.cols(); col++)
		{
			if (is_dowel(row, col) == false)
			{
				shear_vel = (rData.getVel(row, col, 0).x * VON_KARMAN) / log(rParams.pointSpacingZ0() / ROUGH_LEN);
				mLogShear[row][col] = RHO * shear_vel * shear_vel;
			}
		}
}

void BedShear::calcReShear(const Params &rParams, const Data &rData)
{
	for (unsigned int row = 0; row < rParams.rows(); row++)
		for (unsigned int col = 0; col < rParams.cols(); col++)
		{
			if (is_dowel(row, col) == false)
			{
				mReShear[row][col] = -RHO * rData.getRe(row, col, 0).xz;
			}
		}
}


void BedShear::calcTkeShear(const Params &rParams, const TKE &rTke)
{
	for (unsigned int row = 0; row < rParams.rows(); row++)
		for (unsigned int col = 0; col < rParams.cols(); col++)
		{
			if (is_dowel(row, col) == false)
			{
				mTkeShear[row][col] = 0.19 * RHO * rTke.getTke(row, col, 0);	// Biron, et al. 2004
			}
		}
}


void BedShear::calcChezyShear(const Params &rParams, const Data &rData)
{
	for (unsigned int row = 0; row < rParams.rows(); row++)
		for (unsigned int col = 0; col < rParams.cols(); col++)
		{
			if (is_dowel(row, col) == false)
			{
				mChezyShear[row][col] = RHO * GRAV * rData.calcDepthAvrgVel(rParams, row, col).x / (CHEZY * CHEZY);
			}
		}
}


double BedShear::calcAvrg(const Params &rParams, const vector2d &rArg)
{
	double horiz_avrg = 0.0;
	unsigned int n = 0;

	for (unsigned int row = 0; row < rParams.rows(); row++)
		for (unsigned int col = 0; col < rParams.cols(); col++)
		{
			if (is_dowel(row, col) == false)
			{
				horiz_avrg += rArg[row][col];
				n++;
			}
		}

	return horiz_avrg / n;
}


void BedShear::output(const Params &rParams)
{
	// Open output file and set precision
	std::string filename = "out/" + rParams.filePrefix() + "_bed_stress.dat";
	std::ofstream FILE(filename.c_str());
	FILE.precision(2);

	// Write header to file
	FILE	<< std::scientific
			<< "# X\tY\t\tLogShear (" << calcAvrg(rParams, mLogShear) << ")\t"
			<< "ReShear (" << calcAvrg(rParams, mReShear) << ")\t"
			<< "TkeShear (" << calcAvrg(rParams, mTkeShear) << ")\t"
			<< "ChezyShear (" << calcAvrg(rParams, mChezyShear) << ")\n";

	// Output bed shear stress
	for (unsigned int row = 0; row < rParams.rows(); row++)
	{
		for (unsigned int col = 0; col < rParams.cols(); col++)
		{
			FILE.unsetf(std::ios::scientific);
			FILE	<< row*rParams.pointSpacingY() << "\t" << col*rParams.pointSpacingX() << "\t\t"
					<< std::scientific
					<< mLogShear[row][col] << "\t\t"
					<< mReShear[row][col] << "\t\t"
					<< mTkeShear[row][col] << "\t\t"
					<< mChezyShear[row][col] << "\n";
		}
		FILE << "\n";
	}

	// Close file
	FILE.close();
}
