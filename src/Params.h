#ifndef PARAMS_H_
#define PARAMS_H_

#include <string>

#include "GlobalArgs.h"

class Params {
	std::string mFilePrefix;
	unsigned int mRows;			// = 11
	unsigned int mCols;			// = 7
	unsigned int mLayers;
	double mDowelSpacingX;
	double mDowelSpacingY;
	double mPointSpacingX;
	double mPointSpacingY;
	double mPointSpacingZ0;		// = 0.0075;
	double mPointSpacingZ;		// = 0.02;
	double mDepth;
	double mFlowrate;
public:
	// Constructor
	Params();

	// Mutators
	void setFilePrefix( const GlobalArgs& );
	void setPointSpacing();
	void setFlowProp();

	// Accessors
	std::string filePrefix() const { return mFilePrefix ;}
	unsigned int rows() const { return mRows ;}
	unsigned int cols() const { return mCols ;}
	unsigned int layers() const { return mLayers ;}
	double dowelSpacingX() const { return mDowelSpacingX ;}
	double dowelSpacingY() const { return mDowelSpacingY ;}
	double pointSpacingX() const { return mPointSpacingX ;}
	double pointSpacingY() const { return mPointSpacingY ;}
	double pointSpacingZ0() const { return mPointSpacingZ0 ;}
	double pointSpacingZ() const { return mPointSpacingZ ;}
	double depth() const { return mDepth ;}
	double flowrate() const { return mFlowrate ;}
};


#endif /* PARAMS_H_ */
