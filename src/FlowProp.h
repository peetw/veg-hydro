#ifndef FLOWPROP_H_
#define FLOWPROP_H_

#include <cmath>
#include <fstream>

#include "Params.h"
#include "Data.h"
#include "TKE.h"
#include "utils.h"

class FlowProp {
	double mUBulk;			// U = Q/A
	double mUPore;			// U = Q / (h(b - Nr d))
	double mUVol;			// Volume averaged velocity in x direction
	double mVVol;			// Volume averaged velocity in y direction
	double mWVol;			// Volume averaged velocity in z direction
	double mUShearGrav;			// u* = sqrt( g h S )
	double mUShearLog;		// u* = u k / ln(z/k_s)
	double mUShearBed;		// U* = sqrt(Cd_bed * U_0^2)
	double mReBulk;			// Re = U h / v
	double mReDiam;			// Red = U d / v
	double mFroude;			// Fr = U / sqrt(gh)
	double mSVF;			// SVF
	double mN;				//
	double mCdBulk;			// 2 g S (1 – SVF) / (N d U^2)
public:
	// Constructor
	FlowProp();

	// Mutators
	void calcProp( const Params&, const Data&, const TKE& );

	// Accessors
	double uBulk() const { return mUBulk; }
	double uShear() const { return mUShearGrav; }
	double reBulk() const { return mReBulk; }
	double reDiam() const { return mReDiam; }
	double froude() const { return mFroude; }
	double SVF() const { return mSVF; }
	double n() const { return mN; }
	double cdBulk() const { return mCdBulk; }

	// Operations
	void output( const Params& );
};

#endif /* FLOWPROP_H_ */
