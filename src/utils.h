#ifndef UTILS_H_
#define UTILS_H_

// Universal constants
const double PI = 3.14159;
const double GRAV = 9.81;
const double RHO = 1000.0;
const double KIN_VISC = 1.0E-6;
const double VON_KARMAN = 0.41;

// Experimental constants
const double FLUME_WIDTH = 1.2;
const double SLOPE = 0.001;
const double DOWELL_DIAM = 0.0254;

// Experimental parameters
const double ROUGH_LEN = 0.002;
const double CD_BED = 0.01;
const double CHEZY = 34;


// Declarations
bool is_dowel( const unsigned int, const unsigned int );


#endif /* UTILS_H_ */
