#include "utils.h"

bool is_dowel( const unsigned int row, const unsigned int col )
{
	bool check = false;

	if( (row == 0 && (col == 5 || col == 6)) || (row == 1 && col == 6) ||
		(row == 9 && col == 0) || (row == 10 && (col == 0 || col == 1)) )
			check = true;
	
	return check;
}
