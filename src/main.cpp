#include <iostream>

#include "GlobalArgs.h"
#include "Params.h"
#include "Data.h"
#include "FlowProp.h"
#include "TKE.h"
#include "BedShear.h"

int main( int argc, char *argv[] )
{
	// Initialize class for command line options
	GlobalArgs global_args;

	// Parse command line options
	std::cout << "Parsing command line options\n";
	global_args.setGlobalArgs( argc, argv );

	// Initialize class to hold relevant experiment details
	Params params;

	// Determine file prefix
	std::cout << "Getting file prefix\n";
	params.setFilePrefix( global_args );

	// Determine measurement spacing
	std::cout << "Getting measurement point spacing\n";
	params.setPointSpacing();

	// Determine grid dimensions and flow depth
	std::cout << "Getting grid dimensions and flow depth\n";
	params.setFlowProp();

	// Initialize class to hold measured data
	Data data( params );

	// Read in data
	std::cout << "Reading velocity data\n";
	data.readData( params );

	// Calculate turbulent kinetic energy and TKE budget terms
	TKE tke( params );

	if( global_args.calcTke() == true )
	{
		std::cout << "Calculating TKE\n";
		tke.calcTke( params, data );
		std::cout << "Calculating TKE advection\n";
		tke.calcAdvection( params, data );
		std::cout << "Calculating TKE produciton\n";
		tke.calcProduction( params, data );
		std::cout << "Calculating TKE transport\n";
		tke.calcTransport( params, data );

		std::cout << "Outputting TKE terms\n";
		tke.output( params );
	}

	// Initialize flow properties class
	FlowProp flow_prop;

	// Calculate and output flow properties
	std::cout << "Calculating flow properties\n";
	flow_prop.calcProp( params, data, tke );
	std::cout << "Outputting flow properties\n";
	flow_prop.output( params );


	// Initialize bed shear stress class
	BedShear bed_shear(params);

	// Calculate bed shear stress using different methods
	std::cout << "Calculating logarithmic bed shear stress\n";
	bed_shear.calcLogShear(params, data);
	std::cout << "Calculating Reynolds bed shear stress\n";
	bed_shear.calcReShear(params, data);
	std::cout << "Calculating TKE bed shear stress\n";
	bed_shear.calcTkeShear(params, tke);
	std::cout << "Calculating Chezy bed shear stress\n";
	bed_shear.calcChezyShear(params, data);

	// Output bed shear stress data
	std::cout << "Outputting bed shear stress data\n";
	bed_shear.output(params);

	// Program ran successfully
	return 0;
}
