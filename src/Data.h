#ifndef DATA_H_
#define DATA_H_

#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>

#include "vectorTypes.h"
#include "Params.h"
#include "utils.h"

class Data {
	v3dCartesian mMeanVelocities;
	v3dTensor mReynoldsStresses;
	v3dCartesian mFluctTke;
public:
	// Constructors
	Data( const Params& );

	// Mutators
	void readData( const Params& );

	// Accessors
	const Cartesian& getVel( const unsigned int, const unsigned int, const unsigned int ) const;
	const Tensor& getRe( const unsigned int, const unsigned int, const unsigned int ) const;
	const Cartesian& getFluctTke( const unsigned int, const unsigned int, const unsigned int ) const;

	// Operations
	const Cartesian calcDepthAvrgVel(const Params&, const unsigned int, const unsigned int) const;
	double calcVelGrad( const Params&, const char, const char, const unsigned int, const unsigned int, const unsigned int ) const;
	double calcFluctTkeGrad( const Params&, const char, const char, const unsigned int, const unsigned int, const unsigned int ) const;
};


// Inline accessors
inline const Cartesian& Data::getVel( const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	return mMeanVelocities[row][col][layer];
}

inline const Tensor& Data::getRe( const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	return mReynoldsStresses[row][col][layer];
}

inline const Cartesian& Data::getFluctTke( const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	return mFluctTke[row][col][layer];
}

#endif /* DATA_H_ */
