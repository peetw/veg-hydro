#include "Data.h"

Data::Data( const Params &rParams )
{
	// Assign velocity component arrays
	mMeanVelocities = v3dCartesian (rParams.rows(), v2dCartesian(rParams.cols(), v1dCartesian(rParams.layers())));
	mReynoldsStresses = v3dTensor (rParams.rows(), v2dTensor(rParams.cols(), v1dTensor(rParams.layers())));
	mFluctTke = v3dCartesian (rParams.rows(), v2dCartesian(rParams.cols(), v1dCartesian(rParams.layers())));
}

void Data::readData( const Params &rParams )
{
	// Open file
	std::string filename = "inp/" + rParams.filePrefix() + ".csv";
	std::ifstream FILE(filename.c_str());

	// Check file
	if ( !FILE.is_open() )
		exit(EXIT_FAILURE);

	// Discard column headers
	std::string line;
	getline(FILE, line);

	// Read data
	while( getline(FILE, line) )
	{
		std::istringstream ss(line);
		unsigned int i = 1, row = 0, col = 0, layer = 0;
		std::string val;

		while( getline(ss, val, ';') )
		{
			// Poisition
			if( i == 1 )
				row = atoi(val.c_str())-1;
			else if( i == 2 )
				col = atoi(val.c_str())-1;
			else if( i == 3 )
				layer = atoi(val.c_str())-1;

			// Velocities
			else if( i == 4 )
				mMeanVelocities[row][col][layer].x = atof(val.c_str()) / 100.0;
			else if( i == 5 )
				mMeanVelocities[row][col][layer].y = atof(val.c_str()) / 100.0;
			else if( i == 6 )
				mMeanVelocities[row][col][layer].z = atof(val.c_str()) / 100.0;

			// Reynolds stresses
			else if( i == 7 )
				mReynoldsStresses[row][col][layer].xx = atof(val.c_str()) / (100.0*100.0);
			else if( i == 8 )
				mReynoldsStresses[row][col][layer].yy = atof(val.c_str()) / (100.0*100.0);
			else if( i == 9 )
				mReynoldsStresses[row][col][layer].zz = atof(val.c_str()) / (100.0*100.0);
			else if( i == 10 )
			{
				mReynoldsStresses[row][col][layer].xy = atof(val.c_str()) / (100.0*100.0);
				mReynoldsStresses[row][col][layer].yx = atof(val.c_str()) / (100.0*100.0);
			}
			else if( i == 11 )
			{
				mReynoldsStresses[row][col][layer].xz = atof(val.c_str()) / (100.0*100.0);
				mReynoldsStresses[row][col][layer].zx = atof(val.c_str()) / (100.0*100.0);
			}
			else if( i == 12 )
			{
				mReynoldsStresses[row][col][layer].yz = atof(val.c_str()) / (100.0*100.0);
				mReynoldsStresses[row][col][layer].zy = atof(val.c_str()) / (100.0*100.0);
			}

			// Flcutuating velocities multiplied by the TKE
			else if( i == 13 )
				mFluctTke[row][col][layer].x = atof(val.c_str()) / (100.0*100.0*100.0);
			else if( i == 14 )
				mFluctTke[row][col][layer].y = atof(val.c_str()) / (100.0*100.0*100.0);
			else if( i == 15 )
				mFluctTke[row][col][layer].z = atof(val.c_str()) / (100.0*100.0*100.0);

			i++;
		}
	}

	// Close file
	FILE.close();
}


const Cartesian Data::calcDepthAvrgVel(const Params &rParams, const unsigned int row, const unsigned int col) const
{
	// Initialize return struct
	Cartesian depth_avrg_vel;
	unsigned int n = 0;

	for (unsigned int layer = 0; layer < rParams.layers(); layer++)
	{
		depth_avrg_vel.x += mMeanVelocities[row][col][layer].x;
		depth_avrg_vel.y += mMeanVelocities[row][col][layer].y;
		depth_avrg_vel.z += mMeanVelocities[row][col][layer].z;

		n++;
	}

	depth_avrg_vel.x = depth_avrg_vel.x / n;
	depth_avrg_vel.y = depth_avrg_vel.y / n;
	depth_avrg_vel.z = depth_avrg_vel.z / n;

	// Return depth averaged velocities for specified row and column
	return depth_avrg_vel;
}



double Data::calcVelGrad(	const Params &rParams, const char id, const char dir,
							const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	// Check string args ok
	if( (id != 'U' && id != 'V' && id != 'W') || (dir != 'x' && dir != 'y' && dir != 'z') )
		exit(EXIT_FAILURE);

	// Initialize velocities
	double vel_curr;
	double vel_row_m = 0.0;
	double vel_row_p = 0.0;
	double vel_col_m = 0.0;
	double vel_col_p = 0.0;
	double vel_layer_m = 0.0;
	double vel_layer_p = 0.0;

	// Get velocities
	if( id == 'U' )
	{
		vel_curr = mMeanVelocities[row][col][layer].x;

		if(row > 0)
			vel_row_m = mMeanVelocities[row-1][col][layer].x;

		if(row < rParams.rows()-1)
			vel_row_p = mMeanVelocities[row+1][col][layer].x;

		if(col > 0)
			vel_col_m = mMeanVelocities[row][col-1][layer].x;

		if(col < rParams.cols()-1)
			vel_col_p = mMeanVelocities[row][col+1][layer].x;

		if(layer > 0)
			vel_layer_m = mMeanVelocities[row][col][layer-1].x;

		if(layer < rParams.layers()-1)
			vel_layer_p = mMeanVelocities[row][col][layer+1].x;
	}
	else if( id == 'V' )
	{
		vel_curr = mMeanVelocities[row][col][layer].y;

		if(row > 0)
			vel_row_m = mMeanVelocities[row-1][col][layer].y;

		if(row < rParams.rows()-1)
			vel_row_p = mMeanVelocities[row+1][col][layer].y;

		if(col > 0)
			vel_col_m = mMeanVelocities[row][col-1][layer].y;

		if(col < rParams.cols()-1)
			vel_col_p = mMeanVelocities[row][col+1][layer].y;

		if(layer > 0)
			vel_layer_m = mMeanVelocities[row][col][layer-1].y;

		if(layer < rParams.layers()-1)
			vel_layer_p = mMeanVelocities[row][col][layer+1].y;
	}
	else if( id == 'W' )
	{
		vel_curr = mMeanVelocities[row][col][layer].z;

		if(row > 0)
			vel_row_m = mMeanVelocities[row-1][col][layer].z;

		if(row < rParams.rows()-1)
			vel_row_p = mMeanVelocities[row+1][col][layer].z;

		if(col > 0)
			vel_col_m = mMeanVelocities[row][col-1][layer].z;

		if(col < rParams.cols()-1)
			vel_col_p = mMeanVelocities[row][col+1][layer].z;

		if(layer > 0)
			vel_layer_m = mMeanVelocities[row][col][layer-1].z;

		if(layer < rParams.layers()-1)
			vel_layer_p = mMeanVelocities[row][col][layer+1].z;
	}

	// Initialize gradient return value
	double grad = 0.0;

	// Determine gradients
	if( dir == 'x' )
	{
		if( row == rParams.rows()-1 || is_dowel(row+1,col) )
			// Backward difference
			grad = (vel_curr - vel_row_m) / rParams.pointSpacingX();
		else if( row == 0 || is_dowel(row-1,col))
			// Forward difference
			grad = (vel_row_p - vel_curr) / rParams.pointSpacingX();
		else
			// Central difference
			grad = (vel_row_p - vel_row_m) / (2.0*rParams.pointSpacingX());
	}
	else if( dir == 'y' )
	{
		if( col == rParams.cols()-1 || is_dowel(row,col+1) )
			// Backward difference
			grad = (vel_curr - vel_col_m) / rParams.pointSpacingY();
		else if( col == 0 || is_dowel(row,col-1))
			// Forward difference
			grad = (vel_col_p - vel_curr) / rParams.pointSpacingY();
		else
			// Central difference
			grad = (vel_col_p - vel_col_m) / (2.0*rParams.pointSpacingY());
	}
	else if( dir == 'z' )
	{
		if( layer == 0 )
			// Backward difference (Assume no-slip boundary, ie. U=0 on bed)
			grad = vel_curr / rParams.pointSpacingZ0();
		else if( layer == rParams.layers()-1 )
			// Backward difference
			grad = (vel_curr - vel_layer_m) / rParams.pointSpacingZ();
		else
			// Central difference
			grad = (vel_layer_p - vel_layer_m) / (2.0*rParams.pointSpacingZ());
	}

	return grad;
}



double Data::calcFluctTkeGrad(	const Params &rParams, const char id, const char dir,
								const unsigned int row, const unsigned int col, const unsigned int layer ) const
{
	// Check string args ok
	if( (id != 'u' && id != 'v' && id != 'w') || (dir != 'x' && dir != 'y' && dir != 'z') )
		exit(EXIT_FAILURE);

	// Initialize velocities
	double fluct_tke_curr;
	double fluct_tke_row_m = 0.0;
	double fluct_tke_row_p = 0.0;
	double fluct_tke_col_m = 0.0;
	double fluct_tke_col_p = 0.0;
	double fluct_tke_layer_m = 0.0;
	double fluct_tke_layer_p = 0.0;

	// Get velocities
	if( id == 'u' )
	{
		fluct_tke_curr = mFluctTke[row][col][layer].x;

		if(row > 0)
			fluct_tke_row_m = mFluctTke[row-1][col][layer].x;

		if(row < rParams.rows()-1)
			fluct_tke_row_p = mFluctTke[row+1][col][layer].x;

		if(col > 0)
			fluct_tke_col_m = mFluctTke[row][col-1][layer].x;

		if(col < rParams.cols()-1)
			fluct_tke_col_p = mFluctTke[row][col+1][layer].x;

		if(layer > 0)
			fluct_tke_layer_m = mFluctTke[row][col][layer-1].x;

		if(layer < rParams.layers()-1)
			fluct_tke_layer_p = mFluctTke[row][col][layer+1].x;
	}
	else if( id == 'v' )
	{
		fluct_tke_curr = mFluctTke[row][col][layer].y;

		if(row > 0)
			fluct_tke_row_m = mFluctTke[row-1][col][layer].y;

		if(row < rParams.rows()-1)
			fluct_tke_row_p = mFluctTke[row+1][col][layer].y;

		if(col > 0)
			fluct_tke_col_m = mFluctTke[row][col-1][layer].y;

		if(col < rParams.cols()-1)
			fluct_tke_col_p = mFluctTke[row][col+1][layer].y;

		if(layer > 0)
			fluct_tke_layer_m = mFluctTke[row][col][layer-1].y;

		if(layer < rParams.layers()-1)
			fluct_tke_layer_p = mFluctTke[row][col][layer+1].y;
	}
	else if( id == 'w' )
	{
		fluct_tke_curr = mFluctTke[row][col][layer].z;

		if(row > 0)
			fluct_tke_row_m = mFluctTke[row-1][col][layer].z;

		if(row < rParams.rows()-1)
			fluct_tke_row_p = mFluctTke[row+1][col][layer].z;

		if(col > 0)
			fluct_tke_col_m = mFluctTke[row][col-1][layer].z;

		if(col < rParams.cols()-1)
			fluct_tke_col_p = mFluctTke[row][col+1][layer].z;

		if(layer > 0)
			fluct_tke_layer_m = mFluctTke[row][col][layer-1].z;

		if(layer < rParams.layers()-1)
			fluct_tke_layer_p = mFluctTke[row][col][layer+1].z;
	}

	// Initialize gradient return value
	double grad = 0.0;

	// Determine gradients
	if( dir == 'x' )
	{
		if( row == rParams.rows()-1 || is_dowel(row+1,col) )
			// Backward difference
			grad = (fluct_tke_curr - fluct_tke_row_m) / rParams.pointSpacingX();
		else if( row == 0 || is_dowel(row-1,col) )
			// Forward difference
			grad = (fluct_tke_row_p - fluct_tke_curr) / rParams.pointSpacingX();
		else
			// Central difference
			grad = (fluct_tke_row_p - fluct_tke_row_m) / (2.0*rParams.pointSpacingX());
	}
	else if( dir == 'y' )
	{
		if( col == rParams.cols()-1 || is_dowel(row,col+1) )
			// Backward difference
			grad = (fluct_tke_curr - fluct_tke_col_m) / rParams.pointSpacingY();
		else if( col == 0 || is_dowel(row,col-1) )
			// Forward difference
			grad = (fluct_tke_col_p - fluct_tke_curr) / rParams.pointSpacingY();
		else
			// Central difference
			grad = (fluct_tke_col_p - fluct_tke_col_m) / (2.0*rParams.pointSpacingY());
	}
	else if( dir == 'z' )
	{
		if( layer == 0 )
			// Backward difference (Assume no-slip boundary, ie. U=0 on bed)
			grad = fluct_tke_curr / rParams.pointSpacingZ0();
		else if( layer == rParams.layers()-1 )
			// Backward difference
			grad = (fluct_tke_curr - fluct_tke_layer_m) / rParams.pointSpacingZ();
		else
			// Central difference
			grad = (fluct_tke_layer_p - fluct_tke_layer_m) / (2.0*rParams.pointSpacingZ());
	}

	return grad;
}
