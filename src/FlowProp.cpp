#include "FlowProp.h"

FlowProp::FlowProp() :
	mUBulk(0.0),
	mUPore(0.0),
	mUVol(0.0),
	mVVol(0.0),
	mWVol(0.0),
	mUShearGrav(0.0),
	mUShearLog(0.0),
	mUShearBed(0.0),
	mReBulk(0.0),
	mReDiam(0.0),
	mFroude(0.0),
	mSVF(0.0),
	mN(0.0),
	mCdBulk(0.0) {
}

void FlowProp::calcProp( const Params &rParams, const Data &rData, const TKE &rTke )
{
	// Bulk velocity
	mUBulk = rParams.flowrate() / (rParams.depth() * FLUME_WIDTH);

	// Pore velocity
	mUPore = rParams.flowrate() / (rParams.depth() * (FLUME_WIDTH - (FLUME_WIDTH/rParams.dowelSpacingY() - 1)*DOWELL_DIAM));

	// Volume averaged velocities
	mUVol = 0.0;
	mVVol = 0.0;
	mWVol = 0.0;
	unsigned int n = 0;
	for( unsigned int row = 0; row < rParams.rows(); row++ )
		for( unsigned int col = 0; col < rParams.cols(); col++ )
			for( unsigned int layer = 0; layer < rParams.layers(); layer++ )
			{
				if( is_dowel(row,col) == false )
				{
					mUVol += rData.getVel(row,col,layer).x;
					mVVol += rData.getVel(row,col,layer).y;
					mWVol += rData.getVel(row,col,layer).z;
					n++;
				}
			}
	mUVol = mUVol / n;
	mVVol = mVVol / n;
	mWVol = mWVol / n;

	// Shear velocity
	mUShearGrav = sqrt( GRAV * rParams.depth() * SLOPE );

	// Bulk Reynolds number
	mReBulk = ( mUBulk * rParams.depth() ) / KIN_VISC;

	// Stem Reynolds number
	mReDiam = ( mUBulk * DOWELL_DIAM ) / KIN_VISC;

	// Froude number
	mFroude = mUBulk / sqrt( GRAV * rParams.depth() );

	// Solid volume fraction
	mSVF = (2 * PI * DOWELL_DIAM*DOWELL_DIAM / 4.0) / (rParams.dowelSpacingX() * rParams.dowelSpacingY());

	// Number of trees per m^2
	mN = 2.0 / (rParams.dowelSpacingX() * rParams.dowelSpacingY());

	// Bulk drag coefficient
	mCdBulk = 2.0 * GRAV * SLOPE * (1-mSVF) / (mN * DOWELL_DIAM * mUBulk*mUBulk);

	// Shear velocities from logarithmic velocity profile and bed drag
	mUShearLog = 0.0;
	mUShearBed = 0.0;
	n = 0;
	for( unsigned int row = 0; row < rParams.rows(); row++ )
		for( unsigned int col = 0; col < rParams.cols(); col++ )
		{
			if( is_dowel(row,col) == false )
			{
				mUShearLog += (rData.getVel(row,col,0).x * VON_KARMAN) / log(rParams.pointSpacingZ0() / ROUGH_LEN);
				mUShearBed += sqrt(CD_BED * rData.getVel(row,col,0).x*rData.getVel(row,col,0).x);	// King, et al. 2012
				n++;
			}
		}
	mUShearLog = mUShearLog / n;
	mUShearBed = mUShearBed / n;
}


void FlowProp::output( const Params &rParams )
{
	// Open output file
	std::string filename = "out/" + rParams.filePrefix() + "_prop.dat";
	std::ofstream FILE(filename.c_str());

	// Output data
	FILE.setf(std::ios::scientific);
	FILE.precision(3);
	FILE	<< "Flow_depth:\t\t" << rParams.depth() << " m\n"
			<< "Layers:\t\t\t" << rParams.layers() << "\n"
			<< "Flow_rate:\t\t" << rParams.flowrate() << " m3/s\n"
			<< "Bulk_velocity:\t\t" << mUBulk << " m/s\n"
			<< "Pore_velocity:\t\t" << mUPore << " m/s\n"
			<< "Vol_avrg_U_vel:\t\t" << mUVol << " m/s\n"
			<< "Vol_avrg_V_vel:\t\t" << mVVol << " m/s\n"
			<< "Vol_avrg_W_vel:\t\t" << mWVol << " m/s\n"
			<< "Shear_velocity_grav:\t" << mUShearGrav << " m/s\n"
			<< "Shear_velocity_log:\t" << mUShearLog << " m/s\n"
			<< "Shear_velocity_bed:\t" << mUShearBed << " m/s\n"
			<< "Bulk_Reynolds_no:\t" << mReBulk << "\n"
			<< "Stem_Reynolds_no:\t" << mReDiam << "\n"
			<< "Froude_no:\t\t" << mFroude << "\n"
			<< "Solid_volume_fraction:\t" << mSVF << "\n"
			<< "Stems_per_m2:\t\t" << mN << " m-2\n"
			<< "Bulk_drag_coefficient:\t" << mCdBulk;

	// Close file
	FILE.close();
}
