# Main directories
SRCDIR = src
OBJDIR = obj
BINDIR = bin
INPDIR = inp
OUTDIR = out

# Directories to create if not already existant
MKDIR := $(OBJDIR) $(BINDIR) $(INPDIR) $(OUTDIR)

# Binary file
BIN := $(BINDIR)/veg-hydro

# Dependency file
DEP = .depend

# CPP source and object files
CPP_SRC := $(wildcard $(SRCDIR)/*.cpp)
CPP_OBJ := $(patsubst %.cpp,%.o,$(subst $(SRCDIR),$(OBJDIR),$(CPP_SRC)))

# Compiler options
CXXFLAGS = -c -O3 -Wall

# Linker options
LDFLAGS = -O3
LDLIBS =


# NON-FILE TARGETS
.PHONY: all clean

# DEFAULT TARGET
all: $(MKDIR) $(BIN)

# CREATE DIRECTORIES
$(MKDIR):
	mkdir -p $@

# CREATE BINARY
$(BIN): $(CPP_OBJ)
	@$(RM) $(BIN)
	$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o $@
	@echo "\n**** Compiled on: "`date`" ****"

# DETERMINE DEPENDICES
$(DEP): $(CPP_SRC)
	@$(RM) $(DEP)
	@$(CXX) $(CXXFLAGS) -MM $^ >> $(DEP)
	@sed -i 's|.*:|$(OBJDIR)/&|' $(DEP)

-include $(DEP)

# COMPILE - C++
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $< -o $@

# CLEAN PROJECT
clean:
	$(RM) $(BIN)
	$(RM) $(DEP)
	$(RM) $(CPP_OBJ)
